package com.kgm.kpuzzlesearch;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import org.w3c.dom.Node;
import org.w3c.dom.Element;

import android.content.Context;
import android.content.res.AssetManager;

public class KpsPuzzle
{
	KpsBase base;
	KpsInfo info;
	
	ArrayList<KpsLevel> levels;
	
	int actual;
	
	KpsFlexData flexData = null;
	
	public KpsPuzzle()
	{
		base = new KpsBase();
		info = new KpsInfo();
		
		levels = new ArrayList<KpsLevel>();
		
		actual = 0;
	}
	
	public KpsBase getBase()
	{
		return base;
	}
	
	public KpsInfo getInfo()
	{
		return info;
	}
	
	public static KpsPuzzle read(Context c)
	{
		KpsPuzzle puzzle = null;
		
		try
		{
			AssetManager assetManager = c.getAssets();
      InputStream  inputStream = null;
      
      inputStream = assetManager.open("project.xml");
      
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      dbf.setNamespaceAware(false);
      dbf.setValidating(false);
      DocumentBuilder db = dbf.newDocumentBuilder();
      Document doc = db.parse(inputStream);
      
      KpsPuzzle pz = new KpsPuzzle();
      
      pz.flexData = getFlexData(c);
      
      if (pz.flexData == null)
      	return null;
      
      KpsUtil.log_d("Actual level should be " + String.valueOf(pz.flexData.getLevel()));
      
			doc.getDocumentElement().normalize();
			
			Element el = doc.getDocumentElement();
			
			if (el.getNodeName().compareTo("kpuzzlesearch") != 0)
				throw new Exception("Invalid project file.");
			
			NodeList ns = el.getElementsByTagName("base");
			
			if (ns.getLength() > 0)
			{
				Node n = ns.item(0);
				
				Element l = (Element) n;

				String bgImg = l.getAttribute("bgImage");
				String bgSnd = l.getAttribute("bgSound");
				
				pz.base = new KpsBase();
				
				pz.base.getBackground().setImage(KpsUtil.getImage(c, bgImg));
				pz.base.getBackground().setSound(KpsUtil.getSound(c, bgSnd));

				NodeList ni = l.getElementsByTagName("item");
				
				for (int j = 0; j < ni.getLength(); j++)
				{
					n = ni.item(j);
					
					l = (Element) n;
					
					int x = Integer.parseInt(l.getAttribute("x"));
					int y = Integer.parseInt(l.getAttribute("y"));
					int w = Integer.parseInt(l.getAttribute("width"));
					int h = Integer.parseInt(l.getAttribute("height"));
					
					String img = l.getAttribute("image");
					String nam = l.getAttribute("name");
					
					KpsUtil.log_d("Adding base view item " + nam);
					
					KpsItem item = new KpsItem();
					
					item.setImage(KpsUtil.getImage(c, img));
					item.setName(nam);
					item.setPosition(x, y);
					item.setDimension(w,  h);
					
					pz.base.add(item);
				}
			}
			
			ns = el.getElementsByTagName("info");
			
			if (ns.getLength() > 0)
			{
				Node n = ns.item(0);
				
				Element l = (Element) n;

				String bgImg = l.getAttribute("bgImage");
				String bgSnd = l.getAttribute("bgSound");
				
				pz.info.getBackground().setImage(KpsUtil.getImage(c, bgImg));
				//pz.info.getBackground().setSound(KpsUtil.getSound(c, bgSnd));
			}			

			ns = el.getElementsByTagName("level");
			
			for (int i = 0; i < ns.getLength(); i++)
			{
				Node n = ns.item(i);
				
				if (n.getNodeType() != Node.ELEMENT_NODE)
					continue;
				
				Element l = (Element) n;
				
				KpsLevel lv = new KpsLevel();
				
				String bgImg = l.getAttribute("bgImage");
				String bgSnd = l.getAttribute("bgSound");
				String lname = l.getAttribute("name");
				
				lv.getBackground().setImage(KpsUtil.getImage(c, bgImg));
				//lv.getBackground().setSound(KpsUtil.getSound(c, bgSnd));
				
				NodeList hnl = l.getElementsByTagName("hint");
				
				if (hnl.getLength() > 0)
				{
					Element hl = (Element) hnl.item(0);
					
					String hint = hl.getTextContent();
					
					if (hint != null && hint.length() > 0)
						lv.setHint(hint);
				}
				
				NodeList ni = l.getElementsByTagName("item");
				
				for (int j = 0; j < ni.getLength(); j++)
				{
					n = ni.item(j);
					
					l = (Element) n;
					
					int x = Integer.parseInt(l.getAttribute("x"));
					int y = Integer.parseInt(l.getAttribute("y"));
					int w = Integer.parseInt(l.getAttribute("width"));
					int h = Integer.parseInt(l.getAttribute("height"));
					
					String img = l.getAttribute("image");
					String nam = l.getAttribute("name");
					
					KpsItem item = new KpsItem();
					
					item.setImage(KpsUtil.getImage(c, img));
					item.setName(nam);
					item.setPosition(x, y);
					item.setDimension(w,  h);
					
					lv.add(item);
				}
				
				pz.levels.add(lv);
			}
			
			puzzle = pz;
		}
		catch(Exception e)
		{
			KpsUtil.log_d("Open project error: " + e.getMessage());
		}
		
		if (puzzle.flexData.getLevel() < puzzle.levels.size())
		  puzzle.actual = puzzle.flexData.getLevel();
		
		return puzzle;
	}
	
	public static KpsFlexData getFlexData(Context c)
	{
		KpsFlexData fd = null;
		
		File fdir = c.getFilesDir();
		
		KpsUtil.log_d("File dir is " + fdir.getAbsolutePath());

		if (fd == null)
		{
			if (KpsFlexDataFile.exists(c) == false)
			{
				if (KpsFlexDataFile.create(c) == false)
				{
					KpsUtil.log_d("Unable to create flex data file.");
					
					return null;
				}
			}
			
			KpsFlexDataFile fx = new KpsFlexDataFile(c);
			
			fd = new KpsFlexData();
			
			if (fx.read(fd) == false)
			{
				KpsUtil.log_d("Unable to read flex data file.");
				
				return null;
			}
		}
		
		return fd;
	}
	
	public void store(Context c)
	{
		KpsFlexDataFile fd = new KpsFlexDataFile(c);
		
		flexData.setLevel(actual);
		flexData.setLevels(levels.size());
		flexData.setScore(1);
		
		fd.save(flexData);
	}
	
	public boolean resetActual()
	{
		if (levels.size() < 1)
			return false;
		
		actual = 0;
		
		return true;
	}
	
	public KpsLevel getLevel() {
		if (actual >= levels.size())
			return null;

		return levels.get(actual);
	}

	public KpsItem findItem(int x, int y) {
		if (levels.size() < 1)
			return null;

		if (actual >= levels.size())
			return null;

		KpsLevel lv = levels.get(actual);

		return lv.checkItem(x, y);
	}
	
	public boolean toNextLevel()
	{
		if (levels.size() > 0 && actual < levels.size())
		{
			actual++;
			
			return true;
		}
		
		return false;
	}
	
	public boolean isActualLast()
	{
		if (actual == (levels.size() - 1))
			return true;
		
		return false;
	}

	public boolean isLevelFinished() 
	{
		KpsLevel lv = getLevel();
		
		if (lv == null)
		  return false;
		
		if (lv.uncheckCount() > 0)
		  return false;
		
		return true;
	}
}
