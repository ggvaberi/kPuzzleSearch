package com.kgm.kpuzzlesearch;

import android.view.MotionEvent;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.math.*;
import java.lang.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.TextPaint;
import android.view.View;
import android.widget.Button;


public class KpsGameView extends View
{
	public static int  Fails = 5;
	public static long AnimTime = 2000;
	
	Paint paint;
	
	KpsPuzzle puzzle;
	
	KpsItem select = null;
	
	KpsLevel.SuccessListener lvlistener;
	
	Context ctx = null;
	
	Handler handler;
	
	Lock locker = null;
	
	ValueAnimator animator;
	
	double angle = 0.0;

	long time = 0;
	
	int fails = 0;
	
	boolean drawed = true;
	
	public KpsGameView(Context c, KpsLevel.SuccessListener listener)
	{
		super(c);
		
		ctx = c;
		
		KpsActivity ac = (KpsActivity) c;
		
		puzzle = ac.getPuzzle();
		
		lvlistener = listener;
		
		paint = new Paint();
		
		handler = new Handler();
		
		locker = new ReentrantLock();
		
		animator = new ValueAnimator();
		
		this.setOnTouchListener( new OnTouchListener() {
			@Override
      public boolean onTouch(View v, MotionEvent event) {
          if (event.getActionMasked() == MotionEvent.ACTION_DOWN) 
          {
          	onFindItem(event.getX(), event.getY());
          }

          return false;
      }		
		});
	}
	
	public void refresh()
	{
		fails = 0;
	}	
	
	@Override
	protected void onDraw (Canvas canvas) 
	{
	    int w = getWidth();
	    int h = getHeight();
	    int radius = Math.min(w,h)/2 - 10;
	    paint.setARGB(255, 255, 0, 0);
	    canvas.drawCircle(w/2, h/2, radius, paint);
	    
	    KpsUtil.log_d("onDraw " + String.valueOf(w) + " " + String.valueOf(h));
	    
	    KpsLevel lv = puzzle.getLevel();
	    
	    if (lv == null)
	    	return;
	    
	    KpsBackground bg = lv.getBackground();
	    
	    if (bg != null && bg.getImage() != null)
	    {
	    	Bitmap img = bg.getImage();
	    	Rect rcImg = new Rect(0, 0, img.getWidth(), img.getHeight());
	    	Rect rcViw = new Rect(0, 0, w, h);
	    	canvas.drawBitmap(img, rcImg, rcViw, paint);
	    }
	    
	    for (int i = 0; i < lv.getCount(); i++)
	    {
	    	KpsItem item = lv.getItem(i);
	    	
	    	if (item.getChecked() == true)
	    		continue;
	    	else if (item == select)
	    		continue;
	    	
	    	Bitmap img = item.getImage();
	    	
	    	KpsRect rc = item.getRect();
	    	
	    	String nm = item.getName();
	    	
	    	Rect rcImg = new Rect(0, 0, img.getWidth(), img.getHeight());
	    	Rect rcViw = new Rect(rc.x, rc.y, rc.x + rc.w, rc.y + rc.h);
	    	
		    KpsUtil.log_d("onDraw " + String.valueOf(rc.w) + " " + String.valueOf(rc.h));
	    	canvas.drawBitmap(img, rcImg, rcViw, paint);
	    }
	    
	    if (select != null)
	    {
	    	drawSelected(canvas, select);
	    }
	    
	    drawHint(canvas);
	}
	
	private void drawSelected(Canvas canvas, KpsItem item)
	{
		if (item == null)
			return;
		
  	Bitmap img = item.getImage();

  	KpsRect rc = item.getRect();
  	
  	int diff = (int) (5 * Math.cos(angle));
  	
  	Rect rcImg = new Rect(0, 0, img.getWidth(), img.getHeight());
  	Rect rcViw = new Rect(rc.x + diff, rc.y, rc.x + rc.w + diff, rc.y + rc.h);
  	
    KpsUtil.log_d("onDrawSelected " + String.valueOf(rcViw.left) + " " + String.valueOf(rcViw.right));
    
  	canvas.drawBitmap(img, rcImg, rcViw, paint);
	}
	
	private void drawHint(Canvas canvas)
	{
    Paint paintRect = new Paint();
    
    paintRect.setStrokeWidth(5);
    paintRect.setStyle(Paint.Style.STROKE);
    paintRect.setARGB(150, 50, 50, 50);
    canvas.drawRect(100, 400, 700, 460, paintRect);
    paintRect.setStyle(Paint.Style.FILL);
    paintRect.setARGB(150, 150, 150, 150);
    canvas.drawRect(105, 405, 695, 455, paintRect);
    
    Paint paintText = new Paint();
    
    String hint = "Find objects: ";
    
    ArrayList<String> onames = new ArrayList<String>();

    KpsLevel lv = puzzle.getLevel();
    
    if (lv == null)
    	return;

    for (int i = 0; i < lv.getCount(); i++)
    {
    	KpsItem item = lv.getItem(i);
    	
    	if (item.getChecked() == true)
    		continue;
    	
    	String nm = item.getName();
    	
    	onames.add(nm);
    }
    
    for (int i = 0; i < onames.size(); i++)
    {
    	String n = onames.get(i);
    	
    	hint += n;
    	
    	if (i == (onames.size() - 1))
    		hint += ".";
    	else if (n.length() > 0)
    		hint += ",";
    }
    
    RectF bounds = new RectF(100, 400, 700, 460);
		
		TextPaint textPaint = new TextPaint();
		textPaint.setTextSize(20);
		textPaint.setTypeface(KpsActivity.Font);
	  textPaint.setColor(Color.WHITE);
	  textPaint.setTextAlign(Paint.Align.CENTER);

	  float textHeight = textPaint.descent() - textPaint.ascent();
	  float textOffset = (textHeight / 2) - textPaint.descent();
		
		canvas.drawText(hint, bounds.centerX(), 440, textPaint);
	}
	
	void onFindItem(float x, float y)
	{
		if (select != null)
			return;
		
    KpsItem item = puzzle.findItem((int) x, (int) y) ;
    
    if (item == null || item.getChecked() == true)
    {
    	fails++;
    	
    	if (select != null)
    		select = null;
    	
    	if (fails >= Fails)
    	{
    		((KpsActivity)ctx).switchToFails();
    	}
    	
    	return;
    }
    
    fails = 0;
    
    angle = 0.0;
    
    select = item;
    
		item.setChecked(true);
    
    KpsActivity act = (KpsActivity) ctx;
    
    act.playSound("select");
        
    animator.setFloatValues(0.0f, 8.0f);
    animator.setDuration(500);
    animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
    	@Override
    	public void onAnimationUpdate(ValueAnimator animation) {
    		angle += 0.1;
    		invalidate();
    		KpsUtil.log_d("Timer angle: " + String.valueOf(angle));
    	}
    });
    animator.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationEnd(Animator animation) {
      	select = null;
      	
        boolean fin =  puzzle.isLevelFinished();
        
        if (fin == true)
        {
      		KpsActivity ac = (KpsActivity) ctx;
      		
      		ac.switchToNextLevel();
        }
      }
    });
    
    animator.start();

    KpsUtil.log_d("onFindItem X: " + String.valueOf(x) + " Y: " + String.valueOf(y));
    
    invalidate();
	}	
}
