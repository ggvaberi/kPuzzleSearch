package com.kgm.kpuzzlesearch;

import android.graphics.Bitmap;
import android.media.SoundPool;

public class KpsBackground
{
	Bitmap    image;
	KpsSound  sound;

	public KpsBackground()
	{
		
	}
	
	public boolean setImage(Bitmap img)
	{
		image = img;
		
		return true;
	}
	
	public boolean setSound(KpsSound sp)
	{
		sound = sp;
		
		return true;
	}
	
	Bitmap getImage()
	{
		return image;
	}
	
	KpsSound getSound()
	{
		return sound;
	}
}
