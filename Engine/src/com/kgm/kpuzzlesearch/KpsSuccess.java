package com.kgm.kpuzzlesearch;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextPaint;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class KpsSuccess extends View
{
	KpsActivity act = null;
	
	public KpsSuccess(Context c)
	{
		super(c);
	
		act = (KpsActivity) c;
		
		this.setOnTouchListener( new OnTouchListener() {
			@Override
      public boolean onTouch(View v, MotionEvent event) {
				act.switchToBase();
				
        return false;
      }		
		});
	}
	
	@Override
	protected void onDraw (Canvas canvas) 
	{
		Paint paint = new Paint();
		
		int		w				= getWidth();
		int		h				= getHeight();
		
		paint.setStyle(Paint.Style.FILL);
    paint.setColor(Color.BLACK); 
    canvas.drawRect(new Rect(0, 0, w, h), paint);
		
		RectF bounds = new RectF(0, 0, getWidth(), getHeight());
		
		TextPaint textPaint = new TextPaint();
	  textPaint.setColor(Color.GREEN);
	  textPaint.setTextAlign(Paint.Align.CENTER);
	  float textHeight = textPaint.descent() - textPaint.ascent();
	  float textOffset = (textHeight / 2) - textPaint.descent();
		
		textPaint.setTextSize(60);
		textPaint.setTypeface(KpsActivity.Font);
		canvas.drawText("Success", bounds.centerX(), bounds.centerX() - 50, textPaint);

		textPaint.setColor(Color.CYAN);
		textPaint.setTextSize(30);
		canvas.drawText("Tap to continue.", bounds.centerX(), bounds.centerX() + 50, textPaint);
	}
}
