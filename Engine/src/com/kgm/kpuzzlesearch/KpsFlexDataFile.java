package com.kgm.kpuzzlesearch;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import android.content.Context;

public class KpsFlexDataFile
{
	private static String fileName = "kpuzzlesearch.dat";
	
	private Context context;
	
	public KpsFlexDataFile(Context c)
	{
		context = c;
	}
	
	public static boolean exists(Context c)
	{
		File f = new File(c.getFilesDir(), fileName);
		
		return f.exists();
	}
	
	public static boolean create(Context c)
	{
		File f = new File(c.getFilesDir(),fileName);
		
		try
		{
			f.createNewFile();
		}
		catch(Exception e)
		{
			KpsUtil.log_d("Unable to create file " + e.getMessage());
			
			return false;
		}
		
		return f.exists();
	}
	
	public boolean read(KpsFlexData data)
	{
		if (context == null)
			return false;
		
		FileInputStream fis = null;
		
		try
		{
			fis = context.openFileInput(fileName);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			
			String line;
			
			KpsUtil.log_d("Reading flex data...");
			
			while((line = br.readLine()) != null)
			{
	      KpsUtil.log_d("Read flex data line: " + line);
	      
				String[] tokens = line.split(":");
				
        KpsUtil.log_d("Split lines: " + String.valueOf(tokens.length));
        
				if (tokens.length < 2)
					continue;
				
				if (tokens[0].compareTo("level") == 0)
				{
					data.setLevel(Integer.parseInt(tokens[1]));
				}
				else if (tokens[0].compareTo("levels") == 0)
				{
					data.setLevels(Integer.parseInt(tokens[1]));
				}
				else if (tokens[0].compareTo("score") == 0)
				{
					data.setScore(Integer.parseInt(tokens[1]));
				}
			}
		}
		catch(Exception e)
		{
			KpsUtil.log_d("Unable to read file " + e.getMessage());
			//return false;
		}
		finally
		{
		  try {
	      fis.close();
		  } catch (Exception e) {
	      KpsUtil.log_d("Unable to close file " + e.getMessage());
		  }
		}
		
		return true;
	}
	
	public boolean save(KpsFlexData data)
	{
		if (context == null)
			return false;
		
		try
		{
      KpsUtil.log_d("Saving flex data...");
      
      String endl = System.getProperty("line.separator");
      
			FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
			BufferedOutputStream bfos = new BufferedOutputStream(fos);
			
			String line = "level:" + String.valueOf(data.getLevel()) + endl;
			
			bfos.write(line.getBytes());
			
			line = "levels:" + String.valueOf(data.getLevels()) + endl;
			
			bfos.write(line.getBytes());
			
			line = "score:" + String.valueOf(data.getScore()) + endl;
			
			bfos.write(line.getBytes());
			bfos.flush();
			fos.flush();
			fos.close();
		}
		catch(Exception e)
		{
			KpsUtil.log_d("Unable to save to file " + e.getMessage());

			return false;
		}
		
		return true;
	}
}
