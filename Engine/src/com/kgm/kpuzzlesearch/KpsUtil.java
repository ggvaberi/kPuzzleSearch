package com.kgm.kpuzzlesearch;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Looper;
import android.view.View;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.concurrent.Semaphore;

public class KpsUtil
{
	static int sndStreamId = -1;
	
	static boolean loaded = false;
	
	public static final Semaphore semaphore = new Semaphore(0);
	
	public static boolean isLandscape(Activity a)
	{
		if (a == null)
			return false;
		
		if (a.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE)
			return false;
		
		return true;
	}
	
	public static void alert(Context ctx, String alert, String message, final KpsAlertBehaviour bh)
	{
		new AlertDialog.Builder(ctx)
    .setTitle(alert)
    .setMessage(message)
    .setCancelable(false)
    .setPositiveButton("ok", new DialogInterface.OnClickListener() 
    {
        @Override
        public void onClick(DialogInterface dialog, int which) 
        {
        	if (bh != null)
        		bh.call();
        }
    }).show();
	}
	
	public static void log_d(String msg)
	{
		Log.d("DEBUG", "com.kgm.kpuzzlesearch> " + msg);
	}

	public static void log_i(String msg)
	{
		Log.i("INFO", "com.kgm.kpuzzlesearch> " + msg);
	}
	
	public static Bitmap getImage(Context c, String id)
	{
		Bitmap img = null;
		
		if (id == null || id.length() < 1)
			return null;
		
		img = (Bitmap) KpsRes.get(id, KpsRes.TypeImg);
		
		if (img != null)
			return img;
		
		File f = new File("assets/img/", id);
		
		log_i("Loading file " + f.getAbsolutePath());
		
		InputStream is = null;
		
		try
		{
			is = c.getAssets().open("img/" + id);
			
			img = BitmapFactory.decodeStream(is);
			
			is.close();
			
			log_i("Image " + id + " loaded.");
			
			KpsRes.add(img, id);
		}
		catch(Exception e)
		{
			log_d("Cannot load bitmap as " + e.getMessage());
		}
		
		return img;
	}
	
	public static KpsSound getSound(Context c, String id)
	{
		KpsSound snd = null;
		
		if (id == null || id.length() < 1)
			return null;
		
		snd = (KpsSound) KpsRes.get(id, KpsRes.TypeSnd);
		
		if (snd != null)
			return snd;
		
		try
		{
			KpsSound s = new KpsSound();
			
			final SoundPool pool = ((KpsActivity)c).getPlayer();
			final String    sound = "snd/" + id;
			final AssetManager am = c.getAssets(); 
			
			pool.setOnLoadCompleteListener(new OnLoadCompleteListener()
			{
				@Override
				public void onLoadComplete(SoundPool soundPool, int sampleId, int arg2)
				{
  				KpsSound s = KpsRes.getSound(sampleId);
  				
  				if (s != null) 
  				{
  					s.loaded = true;

  					log_d("Sound " + sampleId + " fully loaded.");
  				} 
  				else 
  				{
    				log_d("Sound " + sampleId + " not found in resources.");
  				}
				}
			});
	
			s.id = pool.load(am.openFd(sound), 1);
			
			log_d("Sound " + id + " loaded.");
			
			if (s.id > 0)
			{
				snd = s;
				
				KpsRes.add(snd, id);
			}
		}
		catch(Exception e)
		{
			log_d("Cannot load sound as " + e.getMessage());			
		}
		
		return snd;
	}
	
	public static Typeface getFont(Context c, String id)
	{
		Typeface tf = null;
		
		tf = Typeface.createFromAsset(c.getAssets(), "fnt/" + id);
		
		return tf;
	}
}
