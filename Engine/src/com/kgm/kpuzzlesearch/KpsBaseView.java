package com.kgm.kpuzzlesearch;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.RelativeLayout;

public class KpsBaseView extends ViewGroup
{
	private ImageButton btnStart, btnContinue;
	
	public static Context ctx;
	
	KpsRect rcStart = null, rcContinue = null;
	
	public KpsBaseView(Context c)
	{
		super(c);
		
		ctx = c;

		setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		
		KpsActivity act = (KpsActivity) c;
		
		KpsPuzzle pz = act.getPuzzle();
		
		init(c, pz);
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) 
	{
		final int count = getChildCount();
		
		KpsUtil.log_d("KpsBaseView.onLayout " + String.valueOf(left) + " " + String.valueOf(top) + " " + 
					  String.valueOf(right) + " " + String.valueOf(bottom));
		
		if (btnStart != null)
			btnStart.layout(rcStart.x,  rcStart.y, rcStart.x + rcStart.w, rcStart.y + rcStart.h);
		
		if (btnContinue != null)
			btnContinue.layout(rcContinue.x,  rcContinue.y, rcContinue.x + rcContinue.w, rcContinue.y + rcContinue.h);
	}
	
	void init(Context c, KpsPuzzle pz)
	{
		if (pz == null)
			return;
		
		KpsBackground bg = pz.getBase().getBackground();
		
		if (bg != null)
		{
			Bitmap img = bg.getImage();
			
			setBackgroundDrawable(new BitmapDrawable(img));
		}
		
		for (int i = 0; i < pz.getBase().getCount(); i++)
		{
			KpsItem item = pz.getBase().getItem(i);
			
			ImageButton btn = null;
			KpsRect rc = null;
			
			if (item.getName().compareTo("start") == 0)
			{
				rcStart = item.getRect();

				btnStart = new ImageButton(c);
				btnStart.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						final Context c = KpsBaseView.ctx;
						
						KpsActivity a = (KpsActivity) c;
						
						a.onGameStart();
					}
				});
				
				int h = item.getImage().getHeight();
				int w = item.getImage().getWidth();
				
				btn = btnStart;
				rc = rcStart;
			}
			else if (item.getName().compareTo("continue") == 0)
			{
				rcContinue = item.getRect();

				btnContinue = new ImageButton(c);
				btnContinue.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						KpsActivity a = (KpsActivity) KpsBaseView.ctx;
						
						a.onGameContinue();
					}
				});
				
				btn = btnContinue;
				rc = rcContinue;
			}
			
			if (btn == null || rc == null)
				continue;
			
			btn.setMinimumWidth(rc.w);
			btn.setMinimumHeight(rc.h);
			
			btn.setImageBitmap(item.getImage());
			btn.setPadding(0, 0, 0, 0);
			btn.setBackgroundResource(0);
			btn.setScaleType(ImageView.ScaleType.FIT_XY);
			btn.setVisibility(VISIBLE);
			
			addView(btn);
		}
	}
}
