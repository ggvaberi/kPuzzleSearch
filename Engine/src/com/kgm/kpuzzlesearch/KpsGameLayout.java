package com.kgm.kpuzzlesearch;

import android.view.ViewGroup;
import android.widget.Button;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class KpsGameLayout extends ViewGroup 
{
	private static final int Width = 800;
	private static final int Height = 480;
	
	private KpsGameView view;

	private Button  info = null;

	private Context ctx;
	
	public KpsGameLayout(Context c)
	{
		super(c);
		
		ctx = c;
		
		setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

	  int x = (getWidth() - Width) / 2;
		int y = (getHeight() - Height) / 2;

		view = new KpsGameView(c, new KpsLevel.SuccessListener() {
      @Override
      public void onSuccess() 
      {
        KpsActivity act = (KpsActivity) ctx;
        
        act.switchToNextLevel();
      }
    });
		
		addView(view);
		
		view.setMinimumHeight(Height);
		view.setMinimumWidth(Width);
		
		view.setVisibility(VISIBLE);

		view.layout(x, y, Width, Height);
		
		info = new Button(c);
		
		info.setBackgroundDrawable(new Drawable() {
			@Override
			public void draw(Canvas c)
			{
				Bitmap img = KpsUtil.getImage(ctx, "btnInfo.png");
				
				if (img == null)
					return;
				
				Rect src = new Rect(0, 0, img.getWidth(), img.getHeight());
				Rect dst = new Rect(0, 0, getWidth(), getHeight());
				
				Paint paint = new Paint();
				
				c.drawBitmap(img, src, dst, paint);
			}

			@Override
			public int getOpacity()
			{
				return 0;
			}

			@Override
			public void setAlpha(int arg0)
			{
			}

			@Override
			public void setColorFilter(ColorFilter arg0)
			{
			}
		});

		info.setVisibility(INVISIBLE);
		
		this.addView(info);
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) 
	{
		final int count = getChildCount();

		int width  = right - left;
		int height = bottom - top;
		
		int x = (width - Width) / 2;
		int y = (height - Height) / 2;
		
		KpsUtil.log_d("KpsGameLayout.onLayout " + String.valueOf(left) + " " + String.valueOf(top) + " " + 
					  String.valueOf(right) + " " + String.valueOf(bottom));
		
		view.layout(x, y, Width, Height);
	}
	
	public void refresh()
	{
		view.refresh();

		info.setVisibility(INVISIBLE);
	}
	
	public void update()
	{
		KpsUtil.log_d("KpsGameLayout.update");
		
		view.invalidate();
	}
	
	public void showSuccess()
	{
		
	}
	
	public void showFail()
	{
		info.setText("Failed");
		
		info.setVisibility(VISIBLE);
	}
}
