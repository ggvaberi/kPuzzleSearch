package com.kgm.kpuzzlesearch;

import android.graphics.Rect;

public class KpsRect
{
	public int x, y, w, h;
	
	public KpsRect()
	{
		x = y = w = h = 0;
	}
	
	public KpsRect(int x, int y, int w, int h)
	{
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}
	
	public void set(int x, int y, int w, int h)
	{
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}
	
	public Rect toRect()
	{
		return new Rect(x, y, x + w, y + h);
	}
}
