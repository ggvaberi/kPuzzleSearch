package com.kgm.kpuzzlesearch;

import java.util.ArrayList;

public abstract class KpsVisual
{
	public static final int TypeNone  = 0;
	public static final int TypeBase  = 1;
	public static final int TypeInfo  = 2;
	public static final int TypeLevel = 3;

	KpsBackground 		 bground;
	ArrayList<KpsItem> items;
	
	public KpsVisual()
	{
		bground = new KpsBackground();
		
		items = new ArrayList<KpsItem>(); 
	}
	
	public abstract int getType();
	
	public KpsBackground getBackground()
	{
		return bground;
	}
	
	public boolean add(KpsItem item)
	{
		if (item == null)
			return false;
		
		for(int i = 0; i < items.size(); i++)
		{
			if (item == items.get(i))
				return false;
		}
		
		items.add(item);
		
		return true;
	}
	
	public boolean remove(KpsItem item)
	{
		if (item == null)
			return false;
		
		for(int i = items.size(); i > 0; i--)
		{
			if (item == items.get(i - 1))
			{
				items.remove(i - 1);
				
				return true;
			}
		}
		
		return false;
	}
	
	public int getCount()
	{
		return items.size();
	}
	
	public KpsItem getItem(int i)
	{
		if (i >= items.size())
			return null;
		
		return items.get(i);
	}
}
