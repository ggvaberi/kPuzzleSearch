package com.kgm.kpuzzlesearch;

import android.graphics.Bitmap;

public class KpsItem
{
	Bitmap image;
	KpsRect rect;
	
	String name;
	
	boolean checked;
	
	public KpsItem()
	{
		image = null;
		
		rect = new KpsRect();
		
		name = "";
		
		checked = false;
	}
	
	public void setImage(Bitmap img)
	{
		image = img;
	}
	
	public Bitmap getImage()
	{
		return image;
	}
	
	public void setName(String n)
	{
		name = n;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setPosition(int x, int y)
	{
		rect.x = x;
		rect.y = y;
	}
	
	public void setDimension(int w, int h)
	{
		rect.w = w;
		rect.h = h;
	}
	
	public KpsRect getRect()
	{
		return rect;
	}
	
	public boolean getChecked()
	{
		return checked;
	}
	
	public void setChecked(boolean b)
	{
		checked = b;
	}
}
