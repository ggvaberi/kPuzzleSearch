package com.kgm.kpuzzlesearch;

import java.util.ArrayList;

import android.graphics.Bitmap;

public class KpsRes
{
	public static final int TypeNone = 0;
	public static final int TypeImg  = 1;
	public static final int TypeSnd  = 2;
	
	static ArrayList<KpsRes> resources = new ArrayList<KpsRes>();
	
	Object ob = null;
	String id = null;
	int    tp = 0;
	
	public KpsRes(Bitmap img, String i)
	{
		ob = img;
		id = i;
		tp = TypeImg;
	}
	
	public KpsRes(KpsSound snd, String i)
	{
		ob = snd;
		id = i;
		tp = TypeSnd;
	}
	
	public static boolean have(String id, int type)
	{
		for (int i = 0; i < resources.size(); i++)
		{
			KpsRes r = resources.get(i);
			
			if (r.id == id && r.tp == type)
				return true;
		}
		
		return false;
	}
	
	public static Object get(String id, int type)
	{
	  KpsUtil.log_i("Searching resource " + id + " of type " + String.valueOf(type));
	  
		for (int i = 0; i < resources.size(); i++)
		{
			KpsRes r = resources.get(i);
			
			if ((r.id.compareTo(id) == 0) && r.tp == type)
				return r.ob;
		}
		
		return null;
	}
	
	public static boolean add(Bitmap img, String id)
	{
		if (img == null || id.length() < 1 || get(id, TypeImg) != null)
			return false;
		
    KpsUtil.log_i("Stroring resource " + id + " of type Image");
    
		KpsRes res = new KpsRes(img, id);
		
		resources.add(res);
		
		return true;
	}
	
	public static boolean add(KpsSound snd, String id)
	{
		if (snd == null || id.length() < 1 || get(id, TypeSnd) != null)
			return false;
		
    KpsUtil.log_i("Stroring resource " + id + " of type Sound");
    
		KpsRes res = new KpsRes(snd, id);
		
		resources.add(res);
		
		return true;
	}

	public static KpsSound getSound(int stream)
	{
		for (int i = 0; i < resources.size(); i++)
		{
			KpsRes r = resources.get(i);
			
			if (r.tp != TypeSnd)
				continue;
			
			KpsSound s = (KpsSound) r.ob;
			
			if (s.id == stream)
				return s;
		}
		
		return null;
	}
}
