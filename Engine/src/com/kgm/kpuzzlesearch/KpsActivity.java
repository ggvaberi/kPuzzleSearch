package com.kgm.kpuzzlesearch;

import java.io.File;
import java.util.Map;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

public class KpsActivity extends Activity
{
	private static final int StateBase = 0;
	private static final int StateGame = 1;
	private static final int StateInfo = 2;
	private static final int StateFail = 3;
	private static final int StateSucs = 4;
	
	public static Typeface Font = null;
	
	private KpsPuzzle puzzle = null;
	
	private KpsBaseView   view;
	private KpsGameLayout gameView;
	private KpsFail       viewFail;
	private KpsSuccess    viewSucs;
	
	private Point screenSize;
	
	private int state = StateBase;
	
	private SoundPool player = null;
	
	private KpsSound music = null;
	
	private int musicStream = 0; 

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_kps);
		
		if (KpsUtil.isLandscape(this) == false)
		{
			KpsUtil.log_d("Invalid orientation, non landscape.");
			
			finish();
		}
		else
		{
			KpsUtil.log_i("Correct orientation, landscape.");			
		}
		
    player = new SoundPool(15, AudioManager.STREAM_MUSIC, 0);
    
		if (init() == false)
		{
			KpsUtil.log_d("Invalid initialization.");
			
			finish();
		}
		
		view = new KpsBaseView(this);
		
		viewFail = new KpsFail(this);
		viewSucs = new KpsSuccess(this);
		
		gameView = new KpsGameLayout(this);

		switchToBase();
	}
	
	@Override
	public void onBackPressed() 
	{
		if (state == StateBase)
		{
			final Activity act = this;
			
			KpsUtil.alert(this,  "alert", "exit", new KpsAlertBehaviour() {
				@Override
				public void call()
				{
					if (puzzle != null)
						puzzle.store(act);
					
					stopMusic();
					
					player.release();
					
					finish();
				}
			});
		}
		else
		{
			switchToBase();
		}
	}
	
	private boolean init()
	{
		Font = KpsUtil.getFont(this, "FontA.ttf");
		
		if (Font == null)
		{
			KpsUtil.log_d("Unable to load font.");
			
			return false;
		}
		else
		{
			KpsUtil.log_i("Font loaded.");
		}
		
		Display display = getWindowManager().getDefaultDisplay();
		
		screenSize = new Point();
		
		display.getSize(screenSize);
		
		puzzle = KpsPuzzle.read(this);
		
		if (puzzle == null)
		{
			KpsUtil.log_d("Unable to load search puzzle data.");
			
			return false;
		}
		
		KpsUtil.getSound(this, "fail.wav");
    KpsUtil.getSound(this, "success.wav");
    KpsUtil.getSound(this, "select.wav");
		
		return true;
	}
	
	public void onGameStart()
	{
		if (state == StateBase)
		{
			puzzle.resetActual();
			
			KpsLevel lv = puzzle.getLevel();
			
			lv.refresh();
			
			stopMusic();
			playMusic(lv.getBackground().getSound());
			
			setContentView(gameView);
			
			state = StateGame;

			gameView.refresh();
			gameView.update();
		}
	}

	public void onGameContinue()
	{
		if (state == StateBase)
		{
			KpsLevel lv = puzzle.getLevel();
			
			lv.refresh();
			
			setContentView(gameView);
			
			state = StateGame;
			
			stopMusic();
			playMusic(lv.getBackground().getSound());
			
			gameView.refresh();
			gameView.update();
		}
	}
	
	public KpsPuzzle getPuzzle()
	{
		return puzzle;
	}
	
	public void switchToNextLevel()
	{
		if (puzzle == null)
			return;
		
		if (puzzle.isActualLast() == true)
		{
	    playSound("success");
	    
			setContentView(viewSucs);
			
			puzzle.resetActual();
			
			state = StateSucs;
			
			playMusic(null);
		}
		else if (puzzle.toNextLevel() == true)
		{
			KpsLevel lv = puzzle.getLevel();

			if(lv != null) {
				lv.refresh();

				stopMusic();
				playMusic(lv.getBackground().getSound());
			}
			
			gameView.update();
		}
		else
		{
			setContentView(view);
			
			state = StateBase;
			
			stopMusic();
			playMusic(puzzle.getBase().getBackground().getSound());
		}
		
	  KpsUtil.log_d("Switching on next level...");
	}
	
	public void switchToFails()
	{
	  playSound("fail");
	  
		setContentView(viewFail);
		
		state = StateFail;
		
	  KpsUtil.log_d("Switched to fails.");

	  playMusic(null);
	}
	
	public void switchToBase()
	{
		setContentView(view);
		
		view.invalidate();
		
		state = StateBase;
		
		stopMusic();
		playMusic(puzzle.getBase().getBackground().getSound());
		
	  KpsUtil.log_d("Switched to base.");
	}
	
	public void switchToRepeat()
	{
		KpsLevel lv = puzzle.getLevel();

		if(lv != null) {
			lv.refresh();
			stopMusic();
			playMusic(lv.getBackground().getSound());
		}

		setContentView(gameView);
		
		state = StateGame;
		
		gameView.refresh();
		gameView.update();

	  KpsUtil.log_d("Switched to repeat.");
	}
	
	public SoundPool getPlayer()
	{
	  return player;
	}
	
	public void playSound(String id)
	{
	  KpsSound s = (KpsSound) KpsRes.get(id + ".wav", KpsRes.TypeSnd);
	  
	  if (s == null)
	  {
	    KpsUtil.log_d("Have not sound " + id);
	    
	    return;
	  }
	  
	  int res = player.play(s.id, 1, 1, 0, 0, 1);
	  
	  KpsUtil.log_d("Play result for sound " + id + " is " + String.valueOf(res));
	}
	
  public void playMusic(KpsSound s)
  {
    if (s == null || s.loaded == false)
    {
      KpsUtil.log_d("No music.");
      
      return;
    }

    stopMusic();
    
    musicStream = player.play(s.id, 1, 1, 0, -1, 1);
    
    if (musicStream != 0)
    {
      music = s;
    }
    
    KpsUtil.log_d("Play result for music " + String.valueOf(s.id) + " is " + String.valueOf(musicStream));
  }
  
  private void stopMusic()
  {
    if (music == null)
      return;

    player.stop(musicStream);

    musicStream = 0;
    
    music = null;
  }
}
