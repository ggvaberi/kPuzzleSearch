package com.kgm.kpuzzlesearch;

import android.graphics.Rect;

public class KpsLevel extends KpsVisual
{
  public interface SuccessListener
  {
    void onSuccess();
  };
  
	String hint;
	
	public KpsLevel()
	{
		super();
		
		hint = "";
	}
	
	public int getType()
	{
		return KpsVisual.TypeLevel;
	}
	
	public String getHint()
	{
		return hint;
	}
	
	public void setHint(String s)
	{
		hint = s;
	}
	
	public void refresh()
	{
		if (items.size() < 1)
			return;
		
		for (int i = 0; i < items.size(); i++)
		{
			KpsItem item = items.get(i);
			
			item.setChecked(false);
		}
	}
	
	public KpsItem checkItem(int x, int y)
	{
		if (items.size() < 1)
			return null;
		
		for (int i = 0; i < items.size(); i++) {
			KpsItem item = items.get(i);

			KpsRect r = item.getRect();

			Rect rc = new Rect(r.x, r.y, r.x + r.w, r.y + r.h);

			if (rc.contains((int) x, (int) y))
				return item;
		}
		
		return null;
	}
	
	public int uncheckCount()
	{
		if (items.size() < 1)
			return -1;

		int count = 0;
		
		for (int i = 0; i < items.size(); i++) {
			KpsItem item = items.get(i);

			if(item.getChecked() != true)
				count++;
		}
		
		return count;
	}
}
