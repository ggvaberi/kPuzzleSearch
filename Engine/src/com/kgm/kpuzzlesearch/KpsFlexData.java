package com.kgm.kpuzzlesearch;

public class KpsFlexData
{
	int level;
	int levels;
	
	int score;
	
	public KpsFlexData()
	{
		level = 0;
		
		levels = 0;
		
		score = 0;
	}
	
	public int getLevel()
	{
		return level;
	}
	
	public void setLevel(int l)
	{
		level = l;
	}
	
	public int getLevels()
	{
		return levels;
	}
	
	public void setLevels(int l)
	{
		levels = l;
	}
	
	public int getScore()
	{
		return score;
	}
	
	public void setScore(int s)
	{
		score = s;
	}
}
