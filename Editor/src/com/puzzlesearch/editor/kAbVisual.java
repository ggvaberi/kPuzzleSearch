package com.puzzlesearch.editor;

import java.util.ArrayList;

public abstract class kAbVisual implements kIfVisual
{
	public static final int TypeNone  = 0;
	public static final int TypeBase  = 1;
	public static final int TypeInfo  = 2;
	public static final int TypeLevel = 3;

	protected kBackground background;
	
	protected ArrayList<kItem> items;
	
	public kAbVisual()
	{
		items = new ArrayList<kItem>();
		
		background = new kBackground();
	}
	
	public abstract int getType();
	
	public boolean add(kItem item)
	{
		if (item == null)
			return false;
		
		for(int i = 0; i < items.size(); i++)
		{
			if (item == items.get(i))
				return false;
		}
		
		items.add(item);
		
		return true;
	}
	
	public boolean remove(kItem item)
	{
		if (item == null)
			return false;
		
		for(int i = items.size(); i > 0; i--)
		{
			if (item == items.get(i - 1))
			{
				items.remove(i - 1);
				
				return true;
			}
		}
		
		return false;
	}
	
	public int getCount()
	{
		return items.size();
	}
	
	public kItem getItem(int i)
	{
		if (i >= items.size())
			return null;
		
		return items.get(i);
	}
	
	public kBackground getBackground()
	{
		return background;
	}
}
