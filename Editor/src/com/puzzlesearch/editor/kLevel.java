package com.puzzlesearch.editor;

import java.util.ArrayList;

public class kLevel extends kAbVisual
{
	kPrompt prompt;
	
	String hint;
	
	String name;
	
	public kLevel()
	{
		super();
	}
	
	public int getType()
	{
		return TypeLevel;
	}
	
	String getName()
	{
		return name;
	}
	
	void setName(String n)
	{
		name = n;
	}
	
	String getHint()
	{
		return hint;
	}
	
	void setHint(String h)
	{
		hint = h;
	}
}
