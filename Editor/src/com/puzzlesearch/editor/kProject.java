package com.puzzlesearch.editor;

import javax.swing.JFileChooser;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Dimension;
import java.awt.Point;
import java.io.File;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.util.ArrayList;

public class kProject 
{
	String workspace;
	
	ArrayList<kLevel> levels;

	kLevel actual;
	
	kInfo info;
	
	kBase base;
	
	public kProject()
	{
		levels = new ArrayList<kLevel>();
		
		actual = null;
		
		info = new kInfo();
		
		base = new kBase();
	}
	
	boolean setWorkspace(String folder)
	{
		if (workspace != null && workspace.length() > 0)
		{
			System.out.println("workspace already selected");
			
			return false;
		}
		
		File dir = new File(folder);
		
		if (dir.isDirectory() == false)
		{
			System.out.println("Folder " + folder + " is not directory.");
			
			return false;
		}
		
		String[] files = dir.list();
		
		if (files.length > 0)
		{
			System.out.println("Folder " + folder + " is not empty.");
				
			for (int i = 0; i < files.length; i++)
			{
				System.out.println("Folder item " + files[i] + ".");
			}
			
			return false;
		}
		
		workspace = folder;
		
		return true;
	}
	
	boolean save()
	{
		if (workspace == null || workspace.length() < 1)
		{
			return false;
		}
		
		try
		{
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root element
			Document doc = docBuilder.newDocument();
			Element root = doc.createElement("kpuzzlesearch");
			doc.appendChild(root);

			if (base != null)
			{
				Element el = doc.createElement("base");
				
				if (base.getBackground() != null)
				{
					kBackground bg = base.getBackground();

					el.setAttribute("name", "base");

					String fid = bg.getImageFile();
					
					if (fid != null && fid.length() > 0)
						el.setAttribute("bgImage", fid);
					
					fid = bg.getSoundFile();
					
					if (fid != null && fid.length() > 0)
						el.setAttribute("bgSound", fid);
				}
				
				for (int j = 0; j < base.getCount(); j++)
				{
					kItem item = base.getItem(j);
					
					if (item == null)
						continue;
					
					String name = item.getName();
					
					if (name == null || name.length() < 1)
						continue;
					
					name = name.toLowerCase();
					
					if (name.compareTo("start") != 0 && name.compareTo("continue") != 0)
						continue;
					
					String fid = item.getImageFile();
					
					if (fid != null && fid.length() > 0)
					{
						Element ei = doc.createElement("item");
						
						ei.setAttribute("name", item.getName());
						ei.setAttribute("image", fid);
						
						Point pt = item.getLocation();
						Dimension dim = item.getDimension();
						
						ei.setAttribute("x", String.valueOf(pt.x));
						ei.setAttribute("y", String.valueOf(pt.y));
						ei.setAttribute("width", String.valueOf(dim.width));
						ei.setAttribute("height", String.valueOf(dim.height));
						
						el.appendChild(ei);
					}
				}
				
				root.appendChild(el);
			}
			
			if (info != null)
			{
				Element el = doc.createElement("info");
				
				if (info.getBackground() != null)
				{
					kBackground bg = info.getBackground();

					el.setAttribute("name", "info");

					String fid = bg.getImageFile();
					
					if (fid != null && fid.length() > 0)
						el.setAttribute("bgImage", fid);
					
					fid = bg.getSoundFile();
					
					if (fid != null && fid.length() > 0)
						el.setAttribute("bgSound", fid);
				}
								
				root.appendChild(el);
			}

			for (int i = 0; i < levels.size(); i++)
			{
				kLevel l = levels.get(i);
				
				if (l == null || l.getType() != kAbVisual.TypeLevel)
					continue;
				
				kBackground bg = l.getBackground();
				
				if (bg == null)
					continue;
				
				Element el = doc.createElement("level");
				
				el.setAttribute("name", "level - " + String.valueOf(i));
				
				String fid = bg.getImageFile();
				
				if (fid != null && fid.length() > 0)
					el.setAttribute("bgImage", fid);
				
				fid = bg.getSoundFile();
				
				if (fid != null && fid.length() > 0)
					el.setAttribute("bgSound", fid);
				
				String hint = l.getHint(); 
				
				if (hint != null && hint.length() > 0)
				{
					Element hl = doc.createElement("hint");
					
					hl.setTextContent(hint);
					
					el.appendChild(hl);
				}
				
				for (int j = 0; j < l.getCount(); j++)
				{
					kItem item = l.getItem(j);
					
					if (item == null)
						continue;
					
					fid = item.getImageFile();
					
					if (fid != null && fid.length() > 0)
					{
						Element ei = doc.createElement("item");
						
						ei.setAttribute("name", item.getName());
						ei.setAttribute("image", fid);
						
						Point pt = item.getLocation();
						Dimension dim = item.getDimension();
						
						ei.setAttribute("x", String.valueOf(pt.x));
						ei.setAttribute("y", String.valueOf(pt.y));
						ei.setAttribute("width", String.valueOf(dim.width));
						ei.setAttribute("height", String.valueOf(dim.height));
						
						el.appendChild(ei);
					}
				}
				
				root.appendChild(el);
			}
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(workspace + "/project.xml"));
			transformer.transform(source, result);
		}
		catch(Exception e)
		{
			System.out.println("Error occured while save project. " + e.getMessage());
			
			return false;
		}
		
		return true;
	}
	
	boolean open(String folder)
	{
		if (workspace != null && workspace.length() > 0)
		{
			System.out.println("Workspace alredy set.");

			return false;
		}
		
		File dir = new File(folder);
		
		if (dir.isDirectory() == false)
		{
			System.out.println("Folder " + folder + " is not directory.");
			
			return false;
		}
		
		dir = new File(folder + "/" + "img");
		
		if (dir.exists() == false)
		{
			System.out.println("No images directory.");

			return false;
		}
		
		dir = new File(folder + "/" + "snd");
		
		if (dir.exists() == false)
		{
			System.out.println("No sound directory.");

			return false;
		}
		
		File pro = new File(folder + "/project.xml");
		
		if (pro.exists() == false)
		{
			System.out.println("Cannot find project file.");
			
			return false;
		}
		
		workspace = folder;
		
		if (levels.size() > 0)
			levels.clear();
		
		try
		{
			File f = new File(workspace + "/project.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(f);
			
			doc.getDocumentElement().normalize();
			
			Element el = doc.getDocumentElement();
			
			System.out.println("Element is " + el.getNodeName());
			
			if (el.getNodeName().compareTo("kpuzzlesearch") != 0)
			{
				System.out.println("Invalid project file");
				
				throw new Exception("Invalid project file.");
			}
			
			NodeList ns = el.getElementsByTagName("base");
			
			if (ns.getLength() > 0)
			{
				Node n = ns.item(0);
				
				Element l = (Element) n;

				String bgImg = l.getAttribute("bgImage");
				String bgSnd = l.getAttribute("bgSound");
				
				base.getBackground().setImage(workspace + "/img/" + bgImg);
				base.getBackground().setSound(workspace + "/snd/" + bgSnd);

				NodeList ni = l.getElementsByTagName("item");
				
				for (int j = 0; j < ni.getLength(); j++)
				{
					n = ni.item(j);
					
					l = (Element) n;
					
					int x = Integer.parseInt(l.getAttribute("x"));
					int y = Integer.parseInt(l.getAttribute("y"));
					int w = Integer.parseInt(l.getAttribute("width"));
					int h = Integer.parseInt(l.getAttribute("height"));
					
					String img = l.getAttribute("image");
					
					kItem item = new kItem();
					
					item.setImage(workspace + "/img/" + img);
					item.setPosition(x, y);
					item.setDimension(w,  h);
					
					base.add(item);
				}
			}
			
			ns = el.getElementsByTagName("info");
			
			if (ns.getLength() > 0)
			{
				Node n = ns.item(0);
				
				Element l = (Element) n;

				String bgImg = l.getAttribute("bgImage");
				String bgSnd = l.getAttribute("bgSound");
				
				info.getBackground().setImage(workspace + "/img/" + bgImg);
				info.getBackground().setSound(workspace + "/snd/" + bgSnd);
			}
			
			ns = el.getElementsByTagName("level");
			
			for (int i = 0; i < ns.getLength(); i++)
			{
				Node n = ns.item(i);
				
				if (n.getNodeType() != Node.ELEMENT_NODE)
					continue;
				
				Element l = (Element) n;
				
				kLevel lv = new kLevel();
				
				String bgImg = l.getAttribute("bgImage");
				String bgSnd = l.getAttribute("bgSound");
				String lname = l.getAttribute("name");
				
				lv.getBackground().setImage(workspace + "/img/" + bgImg);
				lv.getBackground().setSound(workspace + "/snd/" + bgSnd);
				lv.setName(lname);
				
				NodeList hnl = l.getElementsByTagName("hint");
				
				if (hnl.getLength() > 0)
				{
					Element hl = (Element) hnl.item(0);
					
					String hint = hl.getTextContent();
					
					if (hint != null && hint.length() > 0)
						lv.setHint(hint);
				}
				
				NodeList ni = l.getElementsByTagName("item");
				
				for (int j = 0; j < ni.getLength(); j++)
				{
					n = ni.item(j);
					
					l = (Element) n;
					
					int x = Integer.parseInt(l.getAttribute("x"));
					int y = Integer.parseInt(l.getAttribute("y"));
					int w = Integer.parseInt(l.getAttribute("width"));
					int h = Integer.parseInt(l.getAttribute("height"));
					
					String img = l.getAttribute("image");
					
					kItem item = new kItem();
					
					item.setImage(workspace + "/img/" + img);
					item.setPosition(x, y);
					item.setDimension(w,  h);
					
					lv.add(item);
				}
				
				levels.add(lv);
			}
		}
		catch(Exception e)
		{
			System.out.println("Error while open project. " + e.getMessage());
			
			return false;
		}
		
		return true;
	}
	
	public static String chooseFolder()
	{
		String folder = "";
		
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		int ret = fc.showOpenDialog(null);
		
		if (ret == JFileChooser.APPROVE_OPTION) 
		{
			File file = fc.getSelectedFile();

			System.out.println("Opening: " + file.getName());

			folder = file.getAbsolutePath();
		} 
		else 
		{
			System.out.println("Open command cancelled by user.");
		}
		
		return folder;
	}
	
	public static String chooseImage()
	{
		String path = "";
		
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fc.setFileFilter(new FileNameExtensionFilter("Image file", "png"));
		
		int ret = fc.showOpenDialog(null);
		
		if (ret == JFileChooser.APPROVE_OPTION) 
		{
			File file = fc.getSelectedFile();

			System.out.println("Opening: " + file.getName());

			path = file.getAbsolutePath();
		} 
		else 
		{
			System.out.println("Open command cancelled by user.");
		}
		
		return path;
	}
	
	public static String chooseSound()
	{
		String path = "";
		
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fc.setFileFilter(new FileNameExtensionFilter("Sound file", "wav"));
		
		int ret = fc.showOpenDialog(null);
		
		if (ret == JFileChooser.APPROVE_OPTION) 
		{
			File file = fc.getSelectedFile();

			System.out.println("Opening: " + file.getName());

			path = file.getAbsolutePath();
		} 
		else 
		{
			System.out.println("Open command cancelled by user.");
		}
		
		return path;
	}
	
	boolean prepareWorkspace()
	{
		if (workspace == null || workspace.length() < 1)
			return false;
		
		File file = new File(workspace + "/" + "img");
		
		if (file.exists() == false)
		{
			if (!file.mkdir())
				return false;
		}
		
		file = new File(workspace + "/" + "snd");
		
		if (file.exists() == false)
		{
			if (!file.mkdir())
				return false;
		}
		
		file = new File(workspace + "/" + "project.xml");
		
		if (file.exists() == false)
		{
			try
			{
				file.createNewFile();
			}
			catch (Exception e)
			{
				return false;
			}
		}
		
		return true;
	}
	
	kLevel select()
	{
		kLevel level = null;
		
		if (levels.size() < 1)
		{
			System.out.println("Project has not levels for choose.");
			
			return null;
		}

		String[] ldata = new String[levels.size()];
		
		for(int i = 0; i < ldata.length; i++)
		{
			ldata[i] = "Level - " + String.valueOf(i);
		}
		
		String s;

		s = (String) JOptionPane.showInputDialog(null, "Choose level", "Choose dialog", JOptionPane.PLAIN_MESSAGE, null, ldata,
		    ldata[0]);
		
		System.out.println("Selected levels is " + s + ".");
		
		for(int i = 0; i < ldata.length; i++)
		{
			String sl = "Level - " + String.valueOf(i);
			
			if (sl.compareTo(s) == 0)
			{
				level = levels.get(i);
				
				actual = level;
				
				break;
			}
		}
		
		return level;
	}
	
	void addLevel()
	{
		kLevel level = new kLevel();
		
		levels.add(level);
	}
	
	void delLevel(kLevel lv)
	{
		if (lv == null)
			return;
		
		for (int i = 0; i < levels.size(); i++)
		{
			if(levels.get(i) == lv)
			{
				levels.remove(i);
				
				break;
			}
		}
	}
	
	void keepImage(String path)
	{
		File f = new File(path);
		
		String local =  workspace + "/img/" + f.getName();
		
		File nf = new File(local);
		
		if (nf.exists())
			return;
		
		kUtil.copyFile(path,  local);
	}
	
	void keepSound(String path)
	{
		File f = new File(path);

		String local =  workspace + "/snd/" + f.getName();
		
		File nf = new File(local);
		
		if (nf.exists())
			return;
		
		kUtil.copyFile(path,  local);
	}
	
	public kInfo getInfo()
	{
		return info;
	}
	
	public kBase getBase()
	{
		return base;
	}
}
