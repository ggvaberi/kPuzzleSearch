package com.puzzlesearch.editor;

public interface kIfVisual
{
	boolean     add(kItem item);
	boolean     remove(kItem item);
	int         getCount();
	kItem       getItem(int i);
	kBackground getBackground();
}
