package com.puzzlesearch.editor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class kUtil 
{
	public static boolean copyFile(String src, String dst)
	{
		File sf = null;
		File df = null;
		
		InputStream is = null;
	    OutputStream os = null;
	    
	    try 
	    {
	    	sf = new File(src);
	    	df = new File(dst);
	        is = new FileInputStream(sf);
	        os = new FileOutputStream(df);
	        
	        byte[] buffer = new byte[1024];
	        
	        int length;
	        
	        while ((length = is.read(buffer)) > 0) 
	        {
	            os.write(buffer, 0, length);
	        }
	        
	    	if(is != null)
	    		is.close();
	    	
	    	if(os != null)
	    		os.close();
	    }
	    catch(Exception e)
	    {
			System.out.println("kUtil: error occured while copy file. " + e.getMessage());
			
			return false;
	    }
	    
		return true;
	}
}
