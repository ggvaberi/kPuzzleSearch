package com.puzzlesearch.editor;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Point;

public class kItem
{
	boolean status = false;
	
	BufferedImage image;
	
	String f_image;
	
	String name;
	
	Rectangle rect;

	public kItem()
	{
		rect = new Rectangle(0, 0, 80, 80);
	}
	
	void setPosition(int x, int y)
	{
		rect.x = x;
		rect.y = y;
	}
	
	void setDimension(int w, int h)
	{
		rect.width  = w;
		rect.height = h;
	}
	
	Dimension getDimension()
	{
		return new Dimension(rect.width, rect.height);
	}
	
	Point getLocation()
	{
		return new Point(rect.x, rect.y);
	}
	
	Image getImage()
	{
		return image;
	}
	
	public boolean setImage(String path)
	{
		File file = null;
		
		try
		{
			file = new File(path);
			
			image = ImageIO.read(file);
		}
		catch(Exception e)
		{
			return false;
		}
		
		f_image = file.getName();
		
		return true;
	}
	
	String getImageFile()
	{
		return f_image;
	}
	
	void setName(String n)
	{
		name = n;
	}
	
	String getName()
	{
		return name;
	}
}
