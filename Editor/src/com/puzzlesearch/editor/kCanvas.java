package com.puzzlesearch.editor;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;


public class kCanvas extends JPanel
{
	//ArrayList<Image> images;
	
	//kLevel level = null;
	kAbVisual level;
	kItem     selected = null;
	
	public kCanvas()
	{
		super(new GridBagLayout());
	}
	
	void clean()
	{
		selected = null;
		
		level = null;
	}
	
	void setLevel(kAbVisual l)
	{
		level = l;
	}
	
	void setSelected(kItem i)
	{
		selected = i;
	}
	
	private void repaintLevel(Graphics g)
	{
		if (g == null)
			g = getGraphics();
		
		if (level == null)
		{
			return;
		}
		
		kBackground bg = level.getBackground(); 
		
		if(bg != null && bg.getImage() != null)
		{
			Image img = bg.getImage();
			
			g.drawImage(img, 0, 0, getWidth(), getHeight(), 
					0, 0, img.getWidth(null), img.getHeight(null), null);
		}
		
		for (int i = 0; i < level.getCount(); i++)
		{
			kItem item = level.getItem(i);
			
			if (item.getImage() == null)
				continue;
			
			Image     img = item.getImage();
			Point     pos = item.getLocation();
			Dimension dim = item.getDimension();
			
			g.drawImage(img, pos.x, pos.y, pos.x + dim.width, pos.y + dim.height, 
						0, 0, img.getWidth(null), img.getHeight(null), null);
			
			if (item == selected)
			{
				g.setColor(Color.red);
				g.drawRect(pos.x, pos.y, dim.width, dim.height);
				g.setColor(Color.gray);
			}
		}
		
		this.level = level;
	}
	
	public void paint(Graphics g) 
	{
		System.out.println("Canvas paint handler.");
		
		g.setColor(Color.gray);
		
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		repaintLevel(g);
    }
}
