package com.puzzlesearch.editor;

import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;

import javax.sound.sampled.AudioInputStream; 
import javax.sound.sampled.AudioSystem; 
import javax.sound.sampled.Clip; 


public class kBackground
{
	BufferedImage image;
	Clip          sound;
	
	String f_image;
	String f_sound;
	
	AudioInputStream audioInputStream;
	
	public kBackground()
	{
		
	}
	
	public boolean setImage(String path)
	{
		File file = null;
		
		try
		{
			file = new File(path);
			
			image = ImageIO.read(file);
		}
		catch(Exception e)
		{
			return false;
		}
		
		f_image = file.getName();
		
		return true;
	}
	
	public boolean setSound(String path)
	{
		File file = null;
		
		try
		{
			file = new File(path);
			
			audioInputStream = AudioSystem.getAudioInputStream(file); 
  
			sound = AudioSystem.getClip();
		}
		catch (Exception e)
		{
			return false;
		}
		
		f_sound = file.getName();
		
		return true;
	}
	
	Image getImage()
	{
		return image;
	}
	
	Clip getSound()
	{
		return sound;
	}
	
	String getImageFile()
	{
		return f_image;
	}
	
	String getSoundFile()
	{
		return f_sound;
	}
}
