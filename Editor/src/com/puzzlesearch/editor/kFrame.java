package com.puzzlesearch.editor;

import javax.swing.*;
import javax.swing.AbstractAction;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.EventObject;

public class kFrame extends JFrame
{
	class MsListener implements MouseListener, MouseMotionListener
	{
		kFrame target; 
		
		Point prevLocation;
		
		public MsListener(kFrame t)
		{
			target = t;
		}
		
		public void	mouseClicked(MouseEvent e)
		{
			System.out.println("MsListener mouseClicked. " + String.valueOf(e.getButton()));
			
			if (e.getButton() == MouseEvent.BUTTON3)
				target.onShowItemMenu(e.getX(), e.getY());
		}
		
		public void  	mouseEntered(MouseEvent e)
		{
		}
		
		public void  	mouseExited(MouseEvent e)
		{
		}
		
		public void  	mousePressed(MouseEvent e)
		{
			System.out.println("MsListener mousePressed.");
			
			Point pt = e.getPoint();
			
			prevLocation = pt;

			target.onSelectItem(pt.x, pt.y);
		}
		
		public void  	mouseReleased(MouseEvent e)
		{
			System.out.println("MsListener mouseReleased.");
		}
		
		public void  	mouseMoved(MouseEvent e)
		{
		}
		
		public void  	mouseDragged(MouseEvent e)
		{
			System.out.println("MsListener mouseDragged.");
			
			Point pt = e.getPoint();
			
			target.onDraggItem(pt.x - prevLocation.x, pt.y - prevLocation.y);
			
			prevLocation = pt;
		}
	}
	
	JMenuBar menuBar;
	
	JPopupMenu menuCanvas;
	
	kProject project;
	
	kAbVisual actual;
	
	kItem    item;
	
	JPanel pLeft = null, pRight = null;
	
	JSplitPane pSplit = null;
	
	kCanvas canvas;
	
	public kFrame()
	{
		super();
		
		menuBar = null;
		project = null;
		actual  = null;
		item    = null;
		
		initMenu();
		
		addWorkspace();
	}
	
	void initMenu()
	{
		if (menuBar != null)
			return;
		
		menuBar = new JMenuBar();

		JMenu mFile = new JMenu("File");
		mFile.add( new JMenuItem(new AbstractAction("New") {
			public void actionPerformed(ActionEvent e) {
		        onFileNew();
		    }
		}));
		mFile.add( new JMenuItem(new AbstractAction("Open") {
			public void actionPerformed(ActionEvent e) {
		        onFileOpen();
		    }
		}));
		mFile.add( new JMenuItem(new AbstractAction("Save") {
			public void actionPerformed(ActionEvent e) {
		        onFileSave();
		    }
		}));
		mFile.add( new JMenuItem(new AbstractAction("Quit") {
			public void actionPerformed(ActionEvent e) {
		        onFileQuit();
		    }
		}));
		
		JMenu mEdit = new JMenu("Edit");
		mEdit.add( new JMenuItem(new AbstractAction("Add") {
			public void actionPerformed(ActionEvent e) {
		        onEditAdd();
		    }
		}));
		mEdit.add( new JMenuItem(new AbstractAction("Remove") {
			public void actionPerformed(ActionEvent e) {
		        onEditDel();
		    }
		}));
		mEdit.add( new JMenuItem(new AbstractAction("Select") {
			public void actionPerformed(ActionEvent e) {
		        onEditSel();
		    }
		}));
		mEdit.add( new JMenuItem(new AbstractAction("Select base") {
			public void actionPerformed(ActionEvent e) {
		        onEditSelBase();
		    }
		}));
		mEdit.add( new JMenuItem(new AbstractAction("Select info") {
			public void actionPerformed(ActionEvent e) {
		        onEditSelInfo();
		    }
		}));
		
		menuBar.add(mFile);
		menuBar.add(mEdit);
		
		setJMenuBar(menuBar);
		
		menuCanvas = new JPopupMenu();
		menuCanvas.add(new JMenuItem(new AbstractAction("Add item") {
			public void actionPerformed(ActionEvent e) {
		        onAddItem();
		    }
		}));
		menuCanvas.add(new JMenuItem(new AbstractAction("Remove item") {
			public void actionPerformed(ActionEvent e) {
		        onRemoveItem();
		    }
		}));
		menuCanvas.add(new JMenuItem(new AbstractAction("Resize item") {
			public void actionPerformed(ActionEvent e) {
		        onResizeItem();
		    }
		}));
		menuCanvas.add(new JMenuItem(new AbstractAction("Rename item") {
			public void actionPerformed(ActionEvent e) {
		        onRenameItem();
		    }
		}));
	}
	
	void onFileNew()
	{
		System.out.println("onFileNew");
		
		canvas.clean();
		
		if (project != null)
		{
			project.save();
			
			project = null;
			
			if (actual != null)
			{
				actual = null;
			}
			
			if (item != null)
			{
				item = null;
			}
		}
		
		String folder = kProject.chooseFolder();

		System.out.println("Project choose folder is " + folder);
		
		if (folder.length() < 1)
		{
			JOptionPane.showMessageDialog(this, "Folder path is invalid.");
			
			return;
		}
		
		project = new kProject();
		
		if (project.setWorkspace(folder) == false)
		{
			JOptionPane.showMessageDialog(this, "Folder cannot be used as project workspase.");
			
			project = null;
		}
		
		if (project.prepareWorkspace() == false)
		{
			JOptionPane.showMessageDialog(this, "Project unable to prepare workspase.");
			
			project = null;
		}
	}
	
	void onFileOpen()
	{
		String folder = kProject.chooseFolder();
		
		kProject prj = new kProject();
		
		if (prj.open(folder) == false)
		{
			JOptionPane.showMessageDialog(this, "Unable to open project.");
			
			return;
		}

		if (project != null)
		{
			project.save();
		}
		
		if (actual != null)
		{
			actual = null;
		}
		
		if (item != null)
		{
			item = null;
		}

		canvas.clean();
		
		project = prj;
		
		actual = project.select();

		canvas.setLevel(actual);
		
		canvas.repaint();
		
		canvas.invalidate();
	}
	
	void onFileSave()
	{
		if (project != null)
			project.save();
	}
	
	void onFileQuit()
	{
		System.exit(0);
	}
	
	void onEditAdd()
	{
		if (project == null)
		{
			JOptionPane.showMessageDialog(this, "Project should open before add level.");
			
			return;
		}
		
		project.addLevel();
	}
	
	void onEditDel()
	{
		if (actual == null || (actual.getType() != kAbVisual.TypeLevel))
		{
			JOptionPane.showMessageDialog(this, "No level is selected.");
			
			return;
		}
		
		project.delLevel((kLevel) actual);
		
		actual = null;
		
		item = null;

		canvas.setLevel(actual);
		
		canvas.repaint();
		
		canvas.invalidate();
	}

	void onEditSel()
	{
		if (project == null)
		{
			JOptionPane.showMessageDialog(this, "Project should open before select level.");
			
			return;
		}
		
		kLevel level = project.select();
		
		if (level == null)
		{
			JOptionPane.showMessageDialog(this, "Invalid level selected.");
			
			return;
		}
		
		actual = level;
		
		if (item != null)
		{
			item = null;
		}
		
		canvas.setLevel(actual);
		
		canvas.repaint();
		
		canvas.invalidate();
	}

	void onEditSelBase()
	{
		if (project == null)
		{
			JOptionPane.showMessageDialog(this, "Project should open before select level.");
			
			return;
		}
		
		kBase base = project.getBase();
		
		actual = base;
		
		item = null;
		
		canvas.setLevel(actual);
		
		canvas.repaint();
		
		canvas.invalidate();
	}
	
	void onEditSelInfo()
	{
		if (project == null)
		{
			JOptionPane.showMessageDialog(this, "Project should open before select level.");
			
			return;
		}
		
		kInfo info = project.getInfo();
		
		actual = info;
		
		item = null;
		
		canvas.setLevel(actual);
		
		canvas.repaint();
		
		canvas.invalidate();
	}
	
	void onSelectBgImage()
	{
		String path = kProject.chooseImage();
		
		if (actual == null)
		{
			JOptionPane.showMessageDialog(this, "No actual level is selected.");
			
			return;
		}
		
		if (item != null)
		{
			item = null;
		}
		
		if (actual.getBackground().setImage(path) == true)
		{
			project.keepImage(path);
			
			canvas.repaint();
			
			canvas.invalidate();
		}
	}

	void onSelectBgSound()
	{
		String path = kProject.chooseSound();
		
		if (actual == null)
		{
			JOptionPane.showMessageDialog(this, "No actual level is selected.");
			
			return;
		}
		
		if (item != null)
		{
			item = null;
		}
		
		if (actual.getBackground().setSound(path) == true)
		{
			project.keepSound(path);
			
			canvas.repaint();
			
			canvas.invalidate();
		}
	}
	
	void onAddItem()
	{
		if (actual == null)
		{
			JOptionPane.showMessageDialog(this, "No actual level is selected.");
			
			return;
		}
		
		if (item != null)
		{
			item = null;
		}
		
		String img = kProject.chooseImage();
		
		if (img != null && img.length() > 0)
		{
			kItem item = new kItem();
			
			if (item.setImage(img) == true)
			{
				if (actual.add(item))
				{
					this.item = item;
					
					project.keepImage(img);
				}
			}
		}
		
		canvas.repaint();
		
		canvas.invalidate();
	}
	
	void onRemoveItem()
	{
		if (item == null)
		{
			JOptionPane.showMessageDialog(this, "No actual item is selected.");
			
			return;
		}

		actual.remove(item);
		
		item = null;
		
		canvas.repaint();
		
		canvas.invalidate();
	}
	
	void onResizeItem()
	{
		if (item == null)
		{
			JOptionPane.showMessageDialog(this, "No actual item is selected.");
			
			return;
		}
		
		JDialog dlg = new JDialog(this, "Resize",  Dialog.ModalityType.DOCUMENT_MODAL);
		dlg.setSize(300, 100);
		dlg.setLayout(new GridLayout(3, 1));
		dlg.add(new JLabel("Resize item", JLabel.CENTER));
		
		SpinnerNumberModel snm = new SpinnerNumberModel(item.getDimension().width, 1, 200, 1);
		
		JSpinner wspn = new JSpinner();
		wspn.setModel(snm);
		
		JSpinner hspn = new JSpinner();
		snm = new SpinnerNumberModel(item.getDimension().height, 1, 200, 1);
		hspn.setModel(snm);
		
		wspn.setSize(200, 30);
		hspn.setSize(200, 30);
		
		wspn.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				Object o = e.getSource();
				JSpinner js = (JSpinner) o;
				
				Dimension dim = item.getDimension();
				
				dim.width = Integer.parseInt((String) js.getValue().toString());
				
				item.setDimension(dim.width, dim.height);
				
				canvas.repaint();
				
				canvas.invalidate();
			}
		});
		
		hspn.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				Object o = e.getSource();
				JSpinner js = (JSpinner) o;
				
				Dimension dim = item.getDimension();
				
				dim.height = Integer.parseInt((String) js.getValue().toString());
				
				item.setDimension(dim.width, dim.height);

				canvas.repaint();
				
				canvas.invalidate();
			}
		});
		
		dlg.add(wspn);
		dlg.add(hspn);
		
		dlg.setVisible(true);
	}
	
	void onRenameItem()
	{
		if (item == null)
		{
			JOptionPane.showMessageDialog(this, "No actual item is selected.");
			
			return;
		}
		
		JDialog dlg = new JDialog(this, "Rename",  Dialog.ModalityType.DOCUMENT_MODAL);
		dlg.setSize(300, 100);
		dlg.setLayout(new GridLayout(3, 1));
		dlg.add(new JLabel("Rename item", JLabel.CENTER));
		JTextField tf = new JTextField(item.getName());
		tf.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				try
				{
					String s = e.getDocument().getText(0, e.getDocument().getLength());					
					item.setName(s);
				}
				catch (Exception ex)
				{
				}
			}
		  public void removeUpdate(DocumentEvent e) {
				try
				{
					String s = e.getDocument().getText(0, e.getDocument().getLength());					
					item.setName(s);
				}
				catch (Exception ex)
				{
				}
		  }
		  public void insertUpdate(DocumentEvent e) {
				try
				{
					String s = e.getDocument().getText(0, e.getDocument().getLength());
					item.setName(s);
				}
				catch (Exception ex)
				{
				}
		  }
		});
		dlg.add(tf);
		
		dlg.setVisible(true);
	}
	
	void onSelectItem(int x, int y)
	{
		System.out.println("onSeletItem " + String.valueOf(x) + " " + String.valueOf(y));

		if (actual == null)
			return;
		
		for (int i = 0; i < actual.getCount(); i++)
		{
			kItem itm = actual.getItem(i);
			
			Point loc = itm.getLocation();
			Dimension dim = itm.getDimension();
			
			Rectangle rect = new Rectangle(loc.x, loc.y, dim.width, dim.height);
			
			System.out.println("onSeletItem rect " + String.valueOf(rect.x) + " " + String.valueOf(rect.y)
			 + " " + String.valueOf(rect.width) + " " + String.valueOf(rect.height));
			
			if (rect.contains(new Point(x, y)) == true)
			{
				this.item = itm;
				
				canvas.setSelected(itm);
				
				canvas.repaint();
				
				canvas.invalidate();
				
				return;
			}
		}
		
		item = null;
		
		canvas.setSelected(item);
		
		canvas.repaint();
		
		canvas.invalidate();
	}
	
	void onDraggItem(int x, int y)
	{
		if (item == null)
			return;
		
		Point loc = item.getLocation();
		
		loc.x += x;
		loc.y += y;
		
		item.setPosition(loc.x, loc.y);
		
		canvas.repaint();
		
		canvas.invalidate();
	}
	
	void onShowItemMenu(int x, int y)
	{
		if (actual == null)
			return;
		
		menuCanvas.show(canvas, x, y);
		
		canvas.revalidate();
	}
	
	void onSetHintText()
	{
		if (actual == null || (actual.getType() != kAbVisual.TypeLevel))
		{
			JOptionPane.showMessageDialog(this, "No actual level is selected.");
			
			return;
		}
		
		JDialog dlg = new JDialog(this, "Set hint data",  Dialog.ModalityType.DOCUMENT_MODAL);
		dlg.setSize(300, 200);
		dlg.setLocation(200,  200);
		
		GridLayout gl = new GridLayout(2, 1);
		
		dlg.setLayout(gl);
		dlg.add(new JLabel("Input hint text.", JLabel.CENTER));
		dlg.setMaximumSize(new Dimension(200, 50));

		JTextArea hint = new JTextArea(((kLevel) actual).getHint());
		
		hint.getDocument().addDocumentListener(new DocumentListener() {

	        @Override
	        public void removeUpdate(DocumentEvent e) 
	        {
	        	try
	        	{
	        		String s = e.getDocument().getText(0, e.getDocument().getLength());
	        		System.out.println("Updated change text: " + s);
	        		
	        		if (actual != null)
	        			((kLevel)actual).setHint(s);
	        	}
	        	catch(Exception x)
	        	{
	        	}
	        }

	        @Override
	        public void insertUpdate(DocumentEvent e) 
	        {
	        	try
	        	{
	        		String s = e.getDocument().getText(0, e.getDocument().getLength());
	        		System.out.println("Updated change text: " + s);
	        		
	        		if (actual != null)
	        			((kLevel) actual).setHint(s);
	        	}
	        	catch(Exception x)
	        	{
	        	}
	        }

	        @Override
	        public void changedUpdate(DocumentEvent e) 
	        {
	        }
	    });
		dlg.add(hint);
		
		dlg.setVisible(true);
	}
	
	void addWorkspace()
	{
		Font font = new Font("Arial", Font.PLAIN, 40);
		
		pLeft = new JPanel();
		pLeft.setBounds(1, 1, 100, 300);
		pLeft.setBackground(Color.cyan);
		pLeft.setFont(font);
		pRight = new JPanel();
		pRight.setBounds(100, 1, 100, 300);
		pRight.setBackground(Color.green);
		
		pSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, pLeft, pRight);
		pSplit.setDividerLocation(100);
		add(pSplit);

		pLeft.add(new JButton(new AbstractAction("bgImage") {
			public void actionPerformed(ActionEvent e) {
				onSelectBgImage();
			}
		}));
		pLeft.add(new JButton(new AbstractAction("bgSound") {
			public void actionPerformed(ActionEvent e) {
				onSelectBgSound();
			}
		}));
		pLeft.add(new JButton(new AbstractAction("textHint") {
			public void actionPerformed(ActionEvent e) {
				onSetHintText();
			}
		}));
		
		Dimension cdim = new Dimension(800, 480);
		
		canvas = new kCanvas();
		
		canvas.setMaximumSize(cdim);
		canvas.setMinimumSize(cdim);
		canvas.setPreferredSize(cdim);
		//canvas.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		//canvas.setAlignmentY(JComponent.CENTER_ALIGNMENT);
		
		//canvas.setBounds(0,  0, 800, 480);
		canvas.setBackground(Color.gray);
		
		MsListener listener = new MsListener(this);
		
		canvas.addMouseListener(listener);
		canvas.addMouseMotionListener(listener);
		
		pRight.add(canvas);
	}
}
